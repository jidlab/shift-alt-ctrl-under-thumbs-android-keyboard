package org.droidtr.keyboard;

import android.content.*;
import android.content.res.*;
import android.os.Build;
import android.graphics.*;
import android.graphics.drawable.*;
import android.inputmethodservice.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;
import android.inputmethodservice.KeyboardView.OnKeyboardActionListener;
import android.provider.Settings ;
import android.net.Uri;
import org.droidtr.keyboard.*;
import org.droidtr.keyboard.CustomKeyboardView.key;
import android.view.inputmethod.EditorInfo;


//4299-4302 bosta
public class IME extends InputMethodService implements OnKeyboardActionListener {
	CustomKeyboardView main=null;
	LinearLayout emoji=null;
	RelativeLayout DroidTR=null;
	LinearLayout chatHead=null;
	RelativeLayout fallbackll=null;
	WindowManager windowManager=null;
	getSettings getSettings=null;
	View nav1,nav2;
	boolean lock=false,caps=false;
	boolean isInit;
	util u=null;
	public RelativeLayout getDroidTR(){
		RelativeLayout ll= new RelativeLayout(this);
		/*
		Emoji interface (was: Emoji arayuzu)
                There are 3 functions here. The first one is for the action that occurs when you press emojis. (was: Burada 3 fonksiyon bulunur. İlki emojilere basinca olusacak eylem icindir)
                The second is the action of the delete key. (was: ikincisi silme tusunun eylemidir.)
                The third one is the return button action. (was: ucuncusu geri dönme tusu eylemidir.)
  		*/
		emoji=new LinearLayout(this);
		emoji.setBackgroundColor(Color.WHITE);
		emoji.setOrientation(LinearLayout.VERTICAL);
		if(getSettings.isEmoji){
		emoji.addView((new emoji(this)).getEmoji(new OnClickListener() {

			@Override
			public void onClick(View p1) {
				if (p1 != null) {
					String emoticon = ((TextView) p1).getText().toString();
					getCurrentInputConnection().commitText(emoticon, 1);
					SharedPreferences read = getSharedPreferences("key", MODE_PRIVATE);
					SharedPreferences.Editor edit = read.edit();
					String[] lastemojis = read.getString("lastemojis", "").split(" ");
					String cache = emoticon;
					for (int i = 0; i < lastemojis.length; i++) {
						if (!lastemojis[i].equals(emoticon) && !lastemojis[i].equals("true") && !lastemojis[i].contains("@")) {
							cache = cache + " " + lastemojis[i];
						}
					}
					edit.putString("lastemojis", cache.trim());
					edit.commit();
				}
			}
		},new OnClickListener(){
			public void onClick(View p1){
				getCurrentInputConnection().sendKeyEvent(new KeyEvent(0, 0, KeyEvent.ACTION_DOWN,KeyEvent.KEYCODE_DEL, 0));
			}
		}, new OnClickListener() {

			@Override
			public void onClick(View p1) {
				emoji.setVisibility(View.GONE);
				main.setVisibility(View.VISIBLE);
				if(chatHead!=null){
					chatHead.setVisibility(View.VISIBLE);
				}
				main.setActiveKeyboard(0);
			}
		}));
		}
		emoji.setVisibility(View.GONE);
		/*
		 Keyboard engine: (was: Klavye motoru:)
                 Layouts for this engine are created according to the following rules: (was: Bu motor icin duzenler su kurallara göre olusturulur:)
                 1-A space is left between each key. (was: her tus arasi bosluk birakilir)
                 2-\n means moving to the next line. (was: alt satira gecme anlamina gelir.)
                 3-Popup interface characters are determined with the comma sign. (was: virgul isareti ile popup arayuzu karakterleri belirlenir.)
                 4-Place a space twice or &b instead of a space. (was: bosyuk yerine iki defa bosluk koyulur veya &b koyulur)
                 5-&v is replace by a comma (was: virgul yerine &v koyulur)
                 &a is replaced with & (from reading replaceAll("&a","&") in app/src/main/java/org/droidtr/keyboard/CustomKeyboardView.java
                 6-To handle capital letters, the UpperCase function in the engine is used. (was: 6-buyuk harfleri ele etmek icin motordaki UpperCase fonksiyonu kullanilir.)
                 Arrays of string type are read with readData and added with addrow(). (was: string turundeki duzenler readData ile okutulur ve addrow ile eklenir.)
)
                 There are separate pages for each layout, with separate uppercase and lowercase letters. (was: Her duzen icin buyuk ve kucuk harfler ayri olmak uzere ayri sayfalar bulunur)
                 Each of these will have an index number. (was: bunlarin birer indis numarasi olur. )
		*/
		main = new CustomKeyboardView(this);
		main.bg=u.gd(getSettings.bg);//#1
		main.bgdown=u.gd(getSettings.bgdown);//#1
		main.bgon=u.gd(getSettings.bgon);//#1
		main.keyboardBackgroundDrawable=getSettings.keyboardBackgroundDrawable;//#1
		main.popupBackgroundDrawable=getSettings.popupBackgroundDrawable;//#1
		main.defaultButtonRadius=getSettings.defaultButtonRadius;//#1
		main.vibration=getSettings.vibration;//#1
		getSettings.loadKeys();//#1
		main.locale=getSettings.locale;//#1
		main.keyboardBackground=getSettings.keyboardBackground;//#1
		main.popupBackground=getSettings.popupBackground;//#1
		main.isAudioEffectEnabled=getSettings.isAudioEffectEnabled;//#1
		main.isVibrationEnabled=getSettings.isVibrationEnabled;//#1
		main.defaultButtonColor=getSettings.defaultButtonColor;//#1
		main.secondaryButtonColor=getSettings.secondaryButtonColor;//#1
		main.secondRowState=getSettings.secondRowState;//#1
		main.secondarySize=getSettings.secondarySize;
		main.primarySize=getSettings.primarySize;
		main.popupDuration=getSettings.popupDuration;
		main.repeatDuration=getSettings.repeatDuration;
		main.otobuyuk=getSettings.otobuyuk;
		if(getSettings.isTabView){
			main.addView(tabView());
		}
		main.createKeyboard(6);


		main.addrow(main.readData(getSettings.rows),0);
		main.addrow(main.UpperCase(main.readData(getSettings.rows)),1);
		main.addrow(main.readData(getSettings.sym),2);
		main.addrow(main.readData(getSettings.sym2),3);

		/*
The getButton function is used to access a button. (was: Bir tusa erisim icin getButton fonksiyonu kullanilir.  This function works based on coordinates. (was: Bu fonksiyon koordinatlara göre calisir.
getButton(page,row,element) (was: getButton(sayfa,sira,eleman)
The page number cannot be negative. (was: sayfa numarasi negatif olamaz.
If the row number is negative, it counts from bottom to top. (was: sira numarasi negatif olursa asağidan yukari olarak sayar.
If the element number is negative, it counts from right to left. (was: eleman numarasi nefatif olursa sağdan sola sayar.

events run as OnTouchListener.  (was: eventler OnTouchListener olarak calisir.)
If there is text on the key, the text is printed on the screen.
If there is no text, the key tag is printed as keyevent. (was: Tus uzerinde yazi varsa yazi ekrana basilir yazi yoksa tus tagi keyevent olarak basilir.)
		*/  

                //Transitions (was: Gecisler)
		main.getButton(1,-1,0).setOnTouchListener(main.setActiveKeyboardEvent(2));
		main.getButton(2,-1,0).setOnTouchListener(main.setActiveKeyboardEvent(0));
		main.getButton(0,-1,0).setOnTouchListener(main.setActiveKeyboardEvent(2));
		main.getButton(2,-2,0).setOnTouchListener(main.setActiveKeyboardEvent(3));
		main.getButton(3,-1,0).setOnTouchListener(main.setActiveKeyboardEvent(0));
		main.getButton(3,-2,0).setOnTouchListener(main.setActiveKeyboardEvent(2));

		if(getSettings.isShiftAvaiable){
			main.getButton(0,-2,0).setOnTouchListener(main.setActiveKeyboardEvent(1,true));
			main.getButton(1,-2,0).setOnTouchListener(main.setActiveKeyboardEvent(0,true));
		}

		//special button (was: özel tuslar)
		main.getButton(0,-2,-1).setTag(KeyEvent.KEYCODE_DEL);
		main.getButton(1,-2,-1).setTag(KeyEvent.KEYCODE_DEL);
		main.getButton(2,-2,-1).setTag(KeyEvent.KEYCODE_DEL);
		main.getButton(3,-2,-1).setTag(KeyEvent.KEYCODE_DEL);

		//Repeat(was: Tekrarlama)
		main.setRepeat(0,-2,-1);
		main.setRepeat(1,-2,-1);
		main.setRepeat(2,-2,-1);
		main.setRepeat(3,-2,-1);
		main.setRepeat(0,0,-1);
		main.setRepeat(1,0,-1);
		main.setRepeat(0,0,0);
		main.setRepeat(1,0,0);

		/*
Theming process (was: Temalandirma islemi
10 different backgrounds can be kept in drawable type. (was: 10 farkli arkaplan drawable turunde tutulabilir.)
Gradient drawable can be created with the gd() function. (was: gd fonksiyonu ile gradient drawable olusturulabilir.)
To theme multiple buttons, an int array array is created. (was: coklu olarak tus temalandirmak icin int array array olusturulur.)
{{page,row,element,number},{page,row,element,number}} (was: {{sayfa,sira,eleman,numara},{sayfa,sira,eleman,numara}})
The page order and element are subject to getButton rules. (was: sayfa sira ve eleman getButton kurallarina tabidir.)
The number is the background number. (was: numara ise arkaplan numarasidir.)
If no background is defined in memory, the default value is returned. (was: Bellekte arkaplan tanimlanmamissa varsayilan değer döner.)

The hp object in the engine represents the default button background. (was: Motordaki bg nesnesi varsayilan tus arkaplanini ifade eder.)
The bg variable must be edited before creating the keyboard layout. (#1) (was: klavye duzeni olusturmadan önce bg değiskeni duzenlenmelidir.(#1))
The bgdown object in the engine is the background that will be changed when pressing the key. (was: Motordaki bgdown nesnesi tusa basarken değistirilecek arkaplandir.)
It must be preset like the bg object. (#1) (was: bg nesnesi gibi önceden ayarlanmalidir. (#1))
The bgon object in the engine is the background that will change when the on/off buttons are activated. (was: Motordaki bgon nesnesi acilip kapanan tuslar etkin olduğunda değisecek arkaplandir.)
It is preset like the bg object. (#1) (was: bg nesnesi gibi önceden ayarlanir. (#1))
		*/
		main.Backgrounds[1]= u.gd(getSettings.primaryButtonColors);
		main.Backgrounds[0] = u.gd(getSettings.secondaryButtonColors);

		int[][] cmds=new int[][]{{0,-1,-1,1},{1,-1,-1,1},{2,-1,-1,1},{3,-1,-1,1}};
		//Theming (was: Temalama)
		main.setKeyBackground(cmds);
		cmds=new int[][]{{0,-1,0,0},{0,-1,1,0},{0,-1,2,0},{0,-1,-2,0},{0,-2,-1,0}};
		main.setKeyBackground(cmds);
		cmds=new int[][]{{1,-1,0,0},{1,-1,1,0},{1,-1,2,0},{1,-1,-2,0},{1,-2,-1,0}};
		main.setKeyBackground(cmds);
		cmds=new int[][]{{2,-2,0,0},{2,-1,0,0},{2,-1,1,0},{2,-1,2,0},{2,-1,3,0},{2,-1,-3,0},{2,-1,-2,0},{2,-2,-1,0}};
		main.setKeyBackground(cmds);
		cmds=new int[][]{{3,-2,0,0},{3,-1,0,0},{3,-1,1,0},{3,-1,2,0},{3,-1,3,0},{3,-1,-3,0},{3,-1,-2,0},{3,-2,-1,0}};
		main.setKeyBackground(cmds);
		main.setKeyBackground(cmds);
		//shift key background (was: shift tusu arkaplani)
		if(getSettings.isShiftAvaiable){
			cmds=new int[][]{{0,-2,0,0},{1,-2,0,0}};
			main.setKeyBackground(cmds);
		}
		/*
Sizing process: (was: Boyutlandirma islemi:)
W() specifies the width and H() specifies the height. (was: W() genisliği H() ile yuksekliği belirtir.)
You can adjust the width and height by proportioning these two values. (was: bu iki değeri oranlayarak genislik ve yukseklik ayarlayabilirsiniz.)
		*/
                //spacebar (was: bosluk tusu)
		float yukseklik = (main.H()*getSettings.heightFactor)/100f; // height, only used once.
		int bosluk = ((int)main.W()*getSettings.spaceLength)/100;
		main.getButton(0,-1,2).setLayoutParams(new LinearLayout.LayoutParams(bosluk,-1));
		main.getButton(1,-1,2).setLayoutParams(new LinearLayout.LayoutParams(bosluk,-1));
		main.getButton(2,-1,-4).setLayoutParams(new LinearLayout.LayoutParams(bosluk,-1));
		main.getButton(3,-1,-4).setLayoutParams(new LinearLayout.LayoutParams(bosluk,-1));
		/*
Latest transactions: (was: Son islemler:)
Page 0 should be made active with setActiveKeyboard. (was: setActiveKeyboard ile 0. sayfa etkin yapilmali)
init() must be run. (Enabling settings #1.) (was: init() calistirilmali. (#1 ayarlarini etkinlestirilyor.))
The keyboard, the popup playout with elephants, and the emoji library should be added, respectively. (was: sirasi ile klavyeyi, filler icine eklenmis popuplayoutu, emoji kutuphaneli eklenmeli.)
		*/

		main.setActiveKeyboard(0);
		main.init();

		if(getSettings.isEmoji){
                         //special buttons icon (was: özel tuslarin simgesi)
			main.getButton(2,-1,-1).setIcon(getResources().getDrawable(R.drawable.emo1));
		}else{
			main.getButton(2,-1,-1).setIcon(getResources().getDrawable(R.drawable.enter));
		}
		//delete key (was: tusu simgesi)
		main.getButton(0,-2,-1).setIcon(getResources().getDrawable(R.drawable.del));
		main.getButton(1,-2,-1).setIcon(getResources().getDrawable(R.drawable.del));
		main.getButton(2,-2,-1).setIcon(getResources().getDrawable(R.drawable.del));
		main.getButton(3,-2,-1).setIcon(getResources().getDrawable(R.drawable.del));

		//enter key (was: tusu simgesi)
		main.getButton(0,-1,-1).setIcon(getResources().getDrawable(R.drawable.enter));
		main.getButton(1,-1,-1).setIcon(getResources().getDrawable(R.drawable.enter));

		//tab key (was: tuşu)
		main.getButton(3,-1,-1).setIcon(getResources().getDrawable(R.drawable.tab));

		//shift key (was: tusu simgesi)
		if(getSettings.isShiftAvaiable){
			main.getButton(0,-2,0).setIcon(getResources().getDrawable(R.drawable.caps));
			main.getButton(1,-2,0).setIcon(getResources().getDrawable(R.drawable.caps));
		}
		main.getButton(2,-2,0).setIcon(getResources().getDrawable(R.drawable.caps));
		main.getButton(3,-2,0).setIcon(getResources().getDrawable(R.drawable.caps));

		//space key (was: tusu simgesi)
		main.getButton(0,-1,-3).setIcon(getResources().getDrawable(R.drawable.space));
		main.getButton(1,-1,-3).setIcon(getResources().getDrawable(R.drawable.space));
		main.getButton(2,-1,-4).setIcon(getResources().getDrawable(R.drawable.space));
		main.getButton(3,-1,-4).setIcon(getResources().getDrawable(R.drawable.space));

		if(getSettings.isEmoji){
			main.getButton(2,-1,-1).setOnTouchListener(main.ViewVisible(emoji,main,chatHead));
		}
		//arrow colors (was: ok tuslari)
		main.getButton(0,0,0).setIcon(getResources().getDrawable(R.drawable.sol));
		main.getButton(1,0,0).setIcon(getResources().getDrawable(R.drawable.sol));
		main.getButton(0,0,-1).setIcon(getResources().getDrawable(R.drawable.sag));
		main.getButton(1,0,-1).setIcon(getResources().getDrawable(R.drawable.sag));

		main.getButton(2,0,0).setIcon(getResources().getDrawable(R.drawable.sol));
		main.getButton(3,0,0).setIcon(getResources().getDrawable(R.drawable.sol));
		main.getButton(2,0,-1).setIcon(getResources().getDrawable(R.drawable.sag));
		main.getButton(3,0,-1).setIcon(getResources().getDrawable(R.drawable.sag));

		getWindow().getWindow().addFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
		if(getSettings.isNavbarEnable && !getSettings.isTablet){
			getWindow().getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
			if (Build.VERSION.SDK_INT > 29){
				getWindow().getWindow().setDecorFitsSystemWindows(false);
				getWindow().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
			                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
			                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
			                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
			                | View.SYSTEM_UI_FLAG_FULLSCREEN
			                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
			}
			nav1=main.getNavbarView();
			nav2=main.getNavbarView();
			main.addView(nav1);
			emoji.addView(nav2);
		}
		main.setHeight((int)(main.H()*getSettings.heightFactor)/100);
		ll.addView(main);
		ll.addView(main.popuplayout(getResources().getDrawable(R.drawable.exit)));
		return ll;
	}
	LinearLayout tabView() {
		final String[] sym = ", . ? \" ' _ - + ; : | & ! \\ / * = % ~ # @ $ ` { } ( ) [ ] < >".split(" ");
		CustomKeyboardView.key[] t = new CustomKeyboardView.key[sym.length];
		LinearLayout lhs = new LinearLayout(this);
		LinearLayout lvl = new LinearLayout(this);
		HorizontalScrollView hsv = new HorizontalScrollView(this);
		LinearLayout ls = new LinearLayout(this);
		for (int i = 1; i != sym.length; i++) {
			final int j = i;
			t[i] = main.generateKey();
			t[i].setOnTouchListener(main.ocl);
			t[i].setLayoutParams(new LinearLayout.LayoutParams(((int)main.WH()/10),((int)main.WH()/10)));
			t[i].setLabel(sym[i]);
			ls.addView(t[i]);
		}
		lvl.addView(lhs);
		lhs.addView(hsv);
		hsv.addView(ls);
		return lvl;
	}
	@Override
	public void onWindowHidden(){
		main.setActiveKeyboard(0);
		main.setVisibility(View.GONE);
		if(chatHead!=null){
			chatHead.setVisibility(View.GONE);
		}
		System.gc();
	}


	@Override
	public void onWindowShown(){
		if (Build.VERSION.SDK_INT >= 23 && getSettings.isTablet) {
			if(!Settings.canDrawOverlays(this)){
				Intent myIntent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,Uri.parse("package:" + getPackageName()));
				startActivity(myIntent);
				System.exit(0);
			}
		}
		emoji.setVisibility(View.GONE);
		main.setVisibility(View.VISIBLE);
		if(nav1 != null){
			nav1.setLayoutParams(new LinearLayout.LayoutParams(-1, main.getNavBarSize()));
		}
		if(nav2 != null){
			nav2.setLayoutParams(new LinearLayout.LayoutParams(-1, main.getNavBarSize()));
		}
		if(chatHead!=null){
			chatHead.setVisibility(View.VISIBLE);
		}
		System.gc();
		main.otobuyukevent();
	}
	@Override
	public View onCreateInputView(){
		getSettings = new getSettings(this);
		u=new util();
		chatHead = new LinearLayout(this);
		u.radius=getSettings.defaultButtonRadius;
		u.stroke=getSettings.defaultButtonStroke;
		DroidTR=getDroidTR();
		fallbackll=new RelativeLayout(this);
			if(getSettings.isTablet && isInit){
				System.exit(0);
			}
			if(getSettings.isTablet && !isInit){
				isInit=true;
				try{
					windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
					final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
					WindowManager.LayoutParams.WRAP_CONTENT,
					WindowManager.LayoutParams.WRAP_CONTENT,
					WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
					WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
					PixelFormat.TRANSLUCENT);
					params.gravity = Gravity.CENTER;
					params.x = 0;
					params.y = 100;
					chatHead.setPadding(dpx(6),dpx(35),dpx(6),dpx(6));
					chatHead.setBackgroundDrawable(u.gd(getSettings.defaultButtonColor));
					windowManager.addView(chatHead, params);
					chatHead.setOnTouchListener(new View.OnTouchListener() {
						private int initialX;
						private int initialY;
						private float initialTouchX;
						private float initialTouchY;

						@Override public boolean onTouch(View v, MotionEvent event) {
							switch (event.getAction()) {
								case MotionEvent.ACTION_DOWN:
								// Get current time in nano seconds.

								initialX = params.x;
								initialY = params.y;
								initialTouchX = event.getRawX();
								initialTouchY = event.getRawY();
								break;
								case MotionEvent.ACTION_UP:
								break;
								case MotionEvent.ACTION_MOVE:
								params.x = initialX + (int) (event.getRawX() - initialTouchX);
								params.y = initialY + (int) (event.getRawY() - initialTouchY);
								windowManager.updateViewLayout(chatHead, params);
								break;
							}
								return false;
						}
					});
					chatHead.addView(DroidTR);
				}catch(Exception e){
					fallbackll.addView(DroidTR);
				}
			}else{
				if(!getSettings.isTablet){
					fallbackll.addView(DroidTR);
				}
			}
			fallbackll.addView(emoji);
		return fallbackll;
	}
	@Override 
	public void onCreate() {
		super.onCreate();
		System.gc();
	}
	public void onStartInputView (EditorInfo info, boolean restarting){
		onStartInput(info,restarting);
	}
	public void onStartInput (EditorInfo info, boolean restarting){
		if(main!=null){
			main.actionId=info.imeOptions & (EditorInfo.IME_MASK_ACTION | EditorInfo.IME_FLAG_NO_ENTER_ACTION);
		}
	}

//KeyboardActionListener 
@Override
	public void swipeDown() {
	}

	@Override
	public void swipeLeft() {
	}

	@Override
	public void swipeRight() {
	}

	@Override
	public void swipeUp() {
	}
	public void onText(CharSequence p1) {
	}
	@Override
	public void onKey(int primaryCode, int[] keyCodes) {
	}

	@Override
	public void onRelease(int primaryCode) {
	}
	@Override
	public void onPress(int primaryCode) {
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(getSettings!=null && main !=null){
			if (getSettings.butfunc && this.isInputViewShown() && this.isShowInputRequested()) {
				if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
					main.ctrl=!main.ctrl;
					return true;
				}else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
					main.alt=!main.alt;
					return true;
				}
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	public static float dp(float px){
		return Resources.getSystem().getDisplayMetrics().density * px;
	}
	public static int dpx(float px){
		return (int)(Resources.getSystem().getDisplayMetrics().density * px);
	}
}
