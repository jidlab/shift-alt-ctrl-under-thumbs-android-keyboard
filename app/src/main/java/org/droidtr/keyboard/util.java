package org.droidtr.keyboard;

import android.graphics.*;
import android.graphics.drawable.*;
import org.droidtr.keyboard.getSettings;
import android.content.Context;
import android.os.Build;
public class util{
	public int radius=14;
	public int stroke=3;

	public Drawable gd(int[] col){
		return gd(col,Color.TRANSPARENT);
	}
	public Drawable gd(int[] col,int strokeColor){
		return gd(col,strokeColor,radius);
	}
	public Drawable gd(int[] col,int strokeColor,int radiusSize){
		return gd(col,strokeColor,radiusSize,stroke);
	}
	public Drawable gd(int[] col,int strokeColor,int radiusSize,int strokeSize){
		GradientDrawable gd = new GradientDrawable();
		if(col.length<3 || Build.VERSION.SDK_INT < 16){
			gd.setColor(col[0]);
		}else{
			gd.setColors(col);
		}
		gd.setCornerRadius(radiusSize);
		gd.setStroke(strokeSize,strokeColor);
		return gd.getConstantState().newDrawable();
	}
	public Drawable gd(int col){
		return gd(col,Color.TRANSPARENT);
	}
	public Drawable gd(int col,int strokeColor){
		return gd(col,strokeColor,radius);
	}
	public Drawable gd(int col,int strokeColor,int radiusSize,int strokeSize){
		return gd(new int[]{col},strokeColor,radiusSize,strokeSize);
	}
	public Drawable gd(int col,int strokeColor,int radiusSize){
		int[] cols={col,col};
		return gd(cols,strokeColor,radiusSize);
	}
}
