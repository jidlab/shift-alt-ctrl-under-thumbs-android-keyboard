package org.droidtr.keyboard;
import android.content.*;
import android.content.res.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.inputmethodservice.*;
import android.media.*;
import android.os.*;
import android.view.*;
import android.view.inputmethod.*;
import android.widget.*;
import java.util.*;
public class CustomKeyboardView extends LinearLayout{
	Context ctx=null;
	//non-shared objects (was: paylaşılmayan nesneler)
	public LinearLayout CustomKeyboardView = null;
	private LinearLayout[] main =null;
	private key[][][] b = null;
	private LinearLayout[][] ll=null;
	private LinearLayout popupll =null;
	private int i=0;
	private int[] rmax=new int[50];
	private int[][] bmax=new int[50][50];
	private key[] popupbuts =new key[12];
	private int enabledpopups=0;
	private int mmax=0;
	private int getActiveKeyboard=0;
	private int status=0;
	private long time=0;
	private boolean tmp=false;
	private boolean keylock=false;
	private AudioManager am=null;
	private Vibrator vib=null;
	private float H=0f;
	private float W=0f;
	private int navBarHeight=0;
	private boolean isCaps=false;
	private int dotstatus=0;
	//LayoutParams
	private LinearLayout.LayoutParams llbb =new LinearLayout.LayoutParams(-1,-1,1.0f);
	//shared objects (was: paylaşılan nesneler)
	public Drawable keyboardBackgroundDrawable=null;
	public Drawable popupBackgroundDrawable=null;
	public Drawable[] Backgrounds=new Drawable[10];
	public Drawable bg=null;
	public Drawable bgon=null;
	public Drawable bgdown=null;
	public int keyboardBackground=Color.parseColor("#282d31");
	public int popupBackground=Color.parseColor("#282d31");
	public int defaultButtonColor=Color.BLACK;
	public int secondaryButtonColor=Color.BLACK;
	public int defaultButtonRadius=18;
	public Runnable prePop=null;
	public Runnable postPop=null;
	public Runnable preInt=null;
	public Runnable postInt=null;
	public OnTouchListener toucharea=null;
	public boolean ctrl=false;
	public boolean alt=false;
	public int popupDuration=250;
	public int repeatDuration=230;
	private int popupHeight=100;
	private int keyboardHeight=-1;
	private int keyboardWidth=-1;
	public int vibration=35;
	public float primarySize=0.5f;
	public float secondarySize=0.25f;
	public Locale locale=Locale.getDefault();
	public boolean isVibrationEnabled=false;
	public boolean isAudioEffectEnabled=false;
	public boolean secondRowState=true;
	public boolean otobuyuk=true;
	public int  actionId=0;
	//Required objects (was: Gerekli nesneler)
	Random r=new Random(); // TODO: random for initial color pickup.
	Handler h = new Handler();
	CustomKeyboardView(Context c){
		super(c);
		ctx=c;
		CustomKeyboardView=this;
		H=c.getResources().getDisplayMetrics().heightPixels;
		W=c.getResources().getDisplayMetrics().widthPixels;
		main=new LinearLayout[50];
		ll = new LinearLayout[50][50];
		b = new key[50][50][50];
		am = (AudioManager) c.getSystemService(c.AUDIO_SERVICE);
		vib = (Vibrator) c.getSystemService(c.VIBRATOR_SERVICE);
		CustomKeyboardView.setOrientation(LinearLayout.VERTICAL);
	}
	public float W(){
		return W;
	}
	public float H(){
		return H;
	}
	public float HW(){
		if(H<W){
			return W;
		}else{
			return H;
		}
	}
	public float WH(){
		if(H<W){
			return H;
		}else{
			return W;
		}
	}
	public void init(){
		if(keyboardBackgroundDrawable!=null){
			CustomKeyboardView.setBackgroundDrawable(keyboardBackgroundDrawable.getConstantState().newDrawable());
		}else{
			CustomKeyboardView.setBackgroundColor(keyboardBackground);
		}
		CustomKeyboardView.setPadding(5,5,5,5);
		System.gc();
	}
	public void addrow(String[][] rows,int index){
		for(int i=0;i<rows.length;i++){
			addrow(rows[i],index);
		}
	}
	public void addrow(String[][][] rows){
		for(int j=0;j<rows.length;j++){
			for(int i=0;i<rows.length;i++){
				addrow(rows[i],j+mmax);
			}
		}
	}
	public int getActiveKeyboard(){
		return getActiveKeyboard;
	}
	public void createKeyboard(int loop){
		for(i=0;i<loop;i++){
			main[mmax] = new LinearLayout(ctx);
			main[mmax].setLayoutParams(llbb);
			main[mmax].setOrientation(LinearLayout.VERTICAL);
			main[mmax].setVisibility(View.GONE);
			CustomKeyboardView.addView(main[mmax]);
			rmax[mmax]=0;
			mmax=mmax+1;
		}
	}
	public void setHeight(int height){
		height+=navBarHeight;
		keyboardHeight=height;
		popupHeight=(int)(HW()/15);
		CustomKeyboardView.setLayoutParams(new LinearLayout.LayoutParams(keyboardWidth,keyboardHeight,1.0f));
	}
	public void setWidth(int width){
		keyboardWidth=width;
		CustomKeyboardView.setLayoutParams(new LinearLayout.LayoutParams(keyboardWidth,keyboardHeight,1.0f));
	}
	public void createKeyboard(){
		createKeyboard(1);
	}
	public int getKeyboardHeight(int index){
		return main[index].getLayoutParams().height;
	}
	public int getKeyboardWidth(int index){
		return main[index].getLayoutParams().width;
	}
	public void addrow(final String[] keys,final int index){
		ll[index][rmax[index]] = new LinearLayout(ctx);
		bmax[index][rmax[index]]=0;
		for(i=0;i<keys.length;i++){
			key k = new key(ctx);
			String keylabel=decode(keys[i])[0];
			k.setLabel(keylabel);
			if(rmax[index]==2 && secondRowState && index<2){
				ll[index][rmax[index]].setPadding(dpx(14),0,dpx(14),0);
			}
			k.setLayoutParams(llbb);
			k.setOnTouchListener(ocl);
			k.popupList=decode(keys[i]);
			if(k.popupList.length>1){
				k.setSecondaryText(decode(keys[i])[1]);
			}
			b[index][rmax[index]][i]=k;
			bmax[index][rmax[index]]++;
			ll[index][rmax[index]].addView(b[index][rmax[index]][i]);
		}
		ll[index][rmax[index]].setLayoutParams(llbb);
		ll[index][rmax[index]].setGravity(Gravity.CENTER);
		main[index].addView(ll[index][rmax[index]]);
		rmax[index]=rmax[index]+1;
	}
	public key getButton(int keyboard,int row,int index){
		if(keyboard<0){
			keyboard=mmax+keyboard;
		}
		if(row<0){
			row=rmax[keyboard]+row;
		}
		if(index<0){
			index=bmax[keyboard][row]+index;
		}
		if(b[keyboard][row][index]!= null){
			return b[keyboard][row][index];
		}else{
			return new key(ctx);
		}
	}
	public int randomColor(int min,int max){
		return (Color.rgb(max-r.nextInt(min),max-r.nextInt(min),max-r.nextInt(min)));
	}
	private void interract(final View p1){
		final String keylabel=((key)p1).getText().toString();
		if(preInt!=null){
			h.post(preInt);
		}
		h.post(new Runnable(){

			@Override
			public void run()
				{
				if(p1.getTag()!=null){
					int keycode=Integer.parseInt("0"+p1.getTag().toString());
					if(keycode==66 && (actionId != 0 && actionId != 1) && actionId <=7){
						getCurrentInputConnection().performEditorAction(actionId);
					}
					if(ctrl||alt||isCaps){
						getCurrentInputConnection().sendKeyEvent(setctrlalt(keycode));
					}else{
						((InputMethodService) getContext()).sendDownUpKeyEvents(keycode);
					}
				}else{
					int keycode=keylabel.toLowerCase().toCharArray()[0];
					if((ctrl||alt) && (getActiveKeyboard==0 || getActiveKeyboard==1) &&keycode>=97 && keycode <=122 &&keylabel.length()<2){
						getCurrentInputConnection().sendKeyEvent(setctrlalt(keycode-68));
					}else{
						getCurrentInputConnection().commitText(keylabel,1);
					}
				}
				alt=false;
				ctrl=false;
			}
		});
		int keycode=0;
		if(p1.getTag()!=null){
			keycode=Integer.parseInt("0"+p1.getTag().toString());
		}else{
			keycode=keylabel.toLowerCase().toCharArray()[0];
		}
		if(keycode==46 && dotstatus==0){
			dotstatus=1;
		}else if(keycode==32 && dotstatus==1){
			dotstatus=2;
		}
		if(keycode==66){
			otobuyukevent();	
		}else if(status!=2 && getActiveKeyboard==1 && dotstatus!=2){
			setActiveKeyboard(0);
		} 
		if(dotstatus==2){
			otobuyukevent();
			dotstatus=0;		
		}
		System.gc();
		if(postInt!=null){
			h.post(postInt);
		}
	}
	public LinearLayout getRow(int keyboard,int index){
		if(ll[index]!= null){
			return ll[keyboard][index];
		}else{
			return new LinearLayout(ctx);
		}
	}
	public LinearLayout getKeyboard(int index){
		if(main[index]!= null){
			return main[index];
		}else{
			return new LinearLayout(ctx);
		}
	}
	public void setActiveKeyboard(int index){
		setActiveKeyboard(index,false);
	}
	public void setActiveKeyboard(int index,boolean isShift){
		getActiveKeyboard=index;
		isCaps=(index==1 || index==2);
		if(main[index].getVisibility()==View.GONE){
			for(i=0;i<mmax;i++){
				main[i].setVisibility(View.GONE);
			}
			main[index].setVisibility(View.VISIBLE);
		}
		if(!isShift){
			status=0;
		}
	}
	public void otobuyukevent(){
		if(otobuyuk){
			setActiveKeyboard(1,true);
			status=1;
		}
	}
	public KeyEvent setctrlalt(int keycode){
		return setctrlalt(keycode,ctrl,alt,isCaps);
	}
	public KeyEvent setctrlalt(int keycode,boolean ctrl,boolean alt,boolean caps){
		if(!caps){
			if(ctrl){
				if(alt){
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,KeyEvent.META_CTRL_ON |KeyEvent.META_ALT_ON,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				}else{
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,KeyEvent.META_CTRL_ON,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				}
			}else{
				if(alt){
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,KeyEvent.META_ALT_ON,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				}else{ 
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,0,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				}
			}
		}else{
			if(ctrl){ 
				if(alt){
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,KeyEvent.META_CTRL_ON | KeyEvent.META_ALT_ON | KeyEvent.META_SHIFT_ON,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				}else{
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,KeyEvent.META_CTRL_ON | KeyEvent.META_SHIFT_ON ,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				}
			}else {
				if(alt){
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,KeyEvent.META_ALT_ON | KeyEvent.META_SHIFT_ON,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				} else{
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,KeyEvent.META_SHIFT_ON,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				}
			}
		} 
	} 
	public OnTouchListener ocl = new OnTouchListener(){
		@Override
		public boolean onTouch(final View p1, final MotionEvent p2){
			if(p2.getAction() == MotionEvent.ACTION_DOWN && !keylock){

				playClick(p1);
				vibrate();
				keylock=true;
				if(bgdown!=null){
					((key)p1).setBackgroundDrawable(bgdown,false);
				}
				time=System.currentTimeMillis();
				tmp=false;
				h.post(new Runnable(){
					@Override
					public void run(){
						Long newtime=System.currentTimeMillis();
						if(!tmp){
							if(newtime-time<popupDuration){
								h.postDelayed(this,50);
							}else{
								tmp=true;
								LongClick(p1,p2);
							}}
					}
				});
			}else if(p2.getAction() == MotionEvent.ACTION_UP){
				if(!tmp && keylock){
					interract(p1);
				}
				tmp=true;
				if(bg!=null){
					((key)p1).setBackgroundDrawable(((key)p1).backgroundDrawable,false);
				}
				keylock=false;
			}
			return true;
		}
	};
	OnTouchListener setActiveKeyboardEvent(int index){
		return setActiveKeyboardEvent(index,false);
	}
	OnTouchListener setActiveKeyboardEvent(final int index,final boolean isShift){
		return new OnTouchListener(){
			@Override
			public boolean onTouch(final View p1, final MotionEvent p2){
				if(p2.getAction() == MotionEvent.ACTION_DOWN){
					vibrate();
					playClick();
					if(isShift){
						if(status !=1){
							setActiveKeyboard(index,true);
						}
						if(status==0){
							status=1;
						}else if(status==1){
							status=2;
						}else{
							status=0;
						}
					}else{
						setActiveKeyboard(index,false);
					}
				}
				return true;
			}
		};
	}
	public key generateKey(){
		return new key(ctx);
	}
	public static float dp(float px){
		return Resources.getSystem().getDisplayMetrics().density * px;
	}
	public static int dpx(float px){
		return (int)(Resources.getSystem().getDisplayMetrics().density * px);
	}
	//key(?) class (was: tuş classı)
	public class key extends RelativeLayout {
		public TextView tv=null;
		public TextView pv=null;
		public ImageView icon=null;
		public Drawable backgroundDrawable=null;
		public Runnable longClick=null;
		public String[] popupList={};
		public key(Context c){
			super(c);
			tv =new TextView(c);
			pv=new TextView(c);
			icon =new ImageView(c);
			addView(pv);
			addView(tv);
			addView(icon);
			pv.setTextSize((WH()/25)*secondarySize);
			tv.setTextSize((WH()/25)*primarySize);
			setBackgroundDrawable(bg.getConstantState().newDrawable());
			this.setGravity(Gravity.CENTER);
		}
		public void setText(String text){
			tv.setTextColor(defaultButtonColor);
			tv.setText(text);
		}
		public void setLabel(String keylabel){
		if(keylabel.contains("::")){
				this.setTag(keylabel.split("::")[0]);
				this.setText(keylabel.split("::")[1]);
			}else{
				this.setText(keylabel);
			}
		}
		public void setSecondaryText(String text){
			if(Build.VERSION.SDK_INT>11 && !text.contains("::")){
				pv.setText(text);
				pv.setTextColor(secondaryButtonColor);
				pv.setX(WH()/48);
				pv.setY((-1)*(WH()/48));
			}
		}
		public void setIcon(Drawable drawable){
			setIcon(drawable,true);
		}
		public void setIcon(Drawable drawable,boolean filter){
			icon.setLayoutParams(new LayoutParams(dpx(28),dpx(28)));
			tv.setVisibility(View.GONE);
			if(filter){
				drawable.setColorFilter(defaultButtonColor, PorterDuff.Mode.SRC_ATOP);
			}
			icon.setBackgroundDrawable(drawable.getConstantState().newDrawable());
		}
		public ImageView getIcon(){
			return icon;
		}
		public void setIcon(int resource,boolean filter){
			tv.setVisibility(View.GONE);
			setIcon(getResources().getDrawable(resource),filter);
		}
		public void setIcon(int resource){
			setIcon(resource,true);
		}
		public CharSequence getText(){
			return tv.getText();
		}

		public void setBackgroundDrawable(Drawable background,boolean backup){
			super.setBackgroundDrawable(background.getConstantState().newDrawable());
			if(backup){
				backgroundDrawable=background;
			}
		}
		@Override
		public void setBackgroundDrawable(Drawable background){
			backgroundDrawable=background;
			super.setBackgroundDrawable(background.getConstantState().newDrawable());
		}
	}
	public void setKeyBackground(int keyboard,int row,int index,int num){
		if(Backgrounds[i]==null){
			Backgrounds[i]=bg.getConstantState().newDrawable();
		}
		getButton(keyboard,row,index).setLayoutParams(llbb);
		getButton(keyboard,row,index).setBackgroundDrawable(Backgrounds[num].getConstantState().newDrawable());
	}
	public void setKeyBackground(int[][] list){
		for (int i=0;i<list.length;i++){
			setKeyBackground(list[i][0],list[i][1],list[i][2],list[i][3]);
		}
	}
	//sighting (?? was: ic çekiliyor)
	public InputConnection getCurrentInputConnection(){
		return ((InputMethodService) getContext()).getCurrentInputConnection();
	}
	public EditorInfo getCurrentInputEditorInfo(){
		return ((InputMethodService) getContext()).getCurrentInputEditorInfo();
	}
	public void setRepeat(int[][] list){
		for (int i=0;i<list.length;i++){
			setRepeat(list[i][0],list[i][1],list[i][2]);
		}
	}
	public void setRepeat(key k){
	k.setOnTouchListener(new OnTouchListener(){
			@Override
			public boolean onTouch(final View p1, final MotionEvent p2){

				if(p2.getAction() == MotionEvent.ACTION_UP){
					keylock=false;
					if(bg!=null){
						((key)p1).setBackgroundDrawable(((key)p1).backgroundDrawable,false);
					}
				}
				if(p2.getAction() == MotionEvent.ACTION_DOWN && !keylock){
					vibrate();
					playClick();
					keylock=true;
					if(bgdown!=null){
						((key)p1).setBackgroundDrawable(bgdown,false);
					}
					i=0;

					interract(p1);
					time=System.currentTimeMillis();
					h.postDelayed(new Runnable(){
					@Override
					public void run(){
						Long newtime=System.currentTimeMillis();
						if(keylock){
							if(newtime-time>(repeatDuration/2)){
								vibrate();
								playClick();
								interract(p1);
								time=System.currentTimeMillis();
							}
							h.postDelayed(this,50);
						}
					}
					},repeatDuration);
				}
				return true;
			}
		});
	}
	//to make it vibrate (was: tekrarlı hale getirmek için)
	public void setRepeat(int keyboard,int row,int index){
		setRepeat(getButton(keyboard,row,index));
	}
	//ctrl on/off event (was: açma kapatma eventi)
	public OnTouchListener ctrlonoff(){
		return new OnTouchListener(){
			@Override
			public boolean onTouch(View p1, MotionEvent p2){
				if(p2.getAction() == MotionEvent.ACTION_DOWN){
					ctrl=!ctrl;
					vibrate();
					playClick();
					if(ctrl){
						((key)p1).setBackgroundDrawable(bgon,false);
					}
					else{
						((key)p1).setBackgroundDrawable(((key)p1).backgroundDrawable,false);
					}
				}
				return false;
			}
		};
	}
	//alt on/off event (was: açma kapatma eventi)
	public OnTouchListener altonoff(){
		return new OnTouchListener(){
			@Override
			public boolean onTouch(View p1, MotionEvent p2){
				if(p2.getAction() == MotionEvent.ACTION_DOWN){
					ctrl=!ctrl;
					vibrate();
					playClick();
					if(alt){
						((key)p1).setBackgroundDrawable(bgon,false);
					}
					else{
						((key)p1).setBackgroundDrawable(((key)p1).backgroundDrawable,false);
					}
				}
				return false;
			}
		};
	}
	public OnTouchListener ViewVisible(final View w,final View main,final View w2){
		return new OnTouchListener(){
			@Override
			public boolean onTouch(View p1, MotionEvent p2){
				if(p2.getAction() == MotionEvent.ACTION_DOWN){
					w.setVisibility(View.VISIBLE);
					if(main!=null){
						main.setVisibility(View.GONE);
					}
					if(w2!=null){
						w2.setVisibility(View.GONE);
					}
					vibrate();
					playClick();
				}
				return false;
			}
		};
	}
	//popup interface (was: arayüzü)
	View popuplayout(Drawable exitIcon){
		LinearLayout ll= new LinearLayout(ctx);
		popupll =new LinearLayout(ctx);
		LinearLayout altview1 =new LinearLayout(ctx);
		LinearLayout altview2 =new LinearLayout(ctx);
		LinearLayout altview3 =new LinearLayout(ctx);
		key exit = new key(ctx);
		if(exitIcon!=null){
			exitIcon.setColorFilter(defaultButtonColor, PorterDuff.Mode.MULTIPLY);
			exit.setIcon(exitIcon.getConstantState().newDrawable());
		}
		exit.setLayoutParams(new LayoutParams(popupHeight,popupHeight));
		exit.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View p1){
				popupll.setVisibility(View.GONE);
				CustomKeyboardView.setEnabled(true);
				enabledpopups=0;
			}
		});
		altview1.addView(exit);
		ll.setOrientation(LinearLayout.VERTICAL);
		enabledpopups=0;
		ll.setGravity(Gravity.CENTER);
		if(popupBackgroundDrawable!=null){
			ll.setBackgroundDrawable(popupBackgroundDrawable.getConstantState().newDrawable());
		}else{
			ll.setBackgroundColor(popupBackground);
		}
		ll.setPadding(dpx(5),dpx(5),dpx(5),dpx(5));
		altview1.setLayoutParams(new LayoutParams(-2,-1));
		altview2.setLayoutParams(new LayoutParams(-2,-1));
		altview3.setLayoutParams(new LayoutParams(-2,-1));
		for(i=0;i<popupbuts.length-1;i++){
			popupbuts[i]=new key(ctx);
			popupbuts[i].setText("");
			popupbuts[i].setLayoutParams(new LayoutParams(popupHeight,popupHeight));
			popupbuts[i].setOnClickListener(oclp);
			popupbuts[i].setBackgroundDrawable(bg);
			popupbuts[i].setVisibility(View.GONE);
			if(i<3){
				altview1.addView(popupbuts[i]);
			}else if(i<7){
				altview2.addView(popupbuts[i]);
			}else{
				altview3.addView(popupbuts[i]);
			}
		}
		ll.addView(altview1);
		ll.addView(altview2);
		ll.addView(altview3);
		popupll.setLayoutParams(new LinearLayout.LayoutParams(-1,keyboardHeight,1.0f));
		popupll.setGravity(Gravity.CENTER);
		popupll.setVisibility(View.GONE);
		CustomKeyboardView.setEnabled(true);
		popupll.setBackgroundColor(Color.TRANSPARENT);
		popupll.addView(ll);
		if(toucharea!=null){
			popupll.setOnTouchListener(toucharea);
		}
		return popupll;
	}
	//Long press (was: Uzun basma)
	private void LongClick(View p1, MotionEvent p2){
		if(((key)p1).popupList.length>1){
			if(prePop!=null){
				h.post(prePop);
			}
			vibrate();
			playClick(p1);
			if(((key)p1).popupList.length<3){
				if(((key)p1).popupList[1].contains("::")){
					getCurrentInputConnection().sendKeyEvent(setctrlalt(Integer.parseInt("0"+((key)p1).popupList[1].split("::")[0])));
				}else{
					getCurrentInputConnection().commitText(((key)p1).popupList[1],1);
				}
			}else{
				popupll.setVisibility(View.VISIBLE);
				CustomKeyboardView.setEnabled(false);
				for(i=0;i<popupbuts.length-1;i++){
					if(i<((key)p1).popupList.length){
						String keylabel=((key)p1).popupList[i];
						if(keylabel.contains("::")){
							popupbuts[i].setTag(keylabel.split("::")[0]);
							popupbuts[i].setText(keylabel.split("::")[1]);
						}else{
							popupbuts[i].setText(keylabel);
							popupbuts[i].setTag(null);
						}
						popupbuts[i].setVisibility(View.VISIBLE);
						enabledpopups++;
					}else{
						popupbuts[i].setText("");
						popupbuts[i].setTag(null);
						popupbuts[i].setVisibility(View.GONE);
					}
				}
			}
			if(postPop!=null){
				h.post(postPop);
			}
		}else{
			if(((key)p1).longClick!=null){
				h.post(((key)p1).longClick);
			}
		}
	}
	//press events popup (?? was: popup basma eventi)
	OnClickListener oclp = new OnClickListener(){
		@Override
		public void onClick(View p1){
			vibrate();
			playClick();
			interract(p1);
			keylock=false;
			popupll.setVisibility(View.GONE);
			CustomKeyboardView.setEnabled(true);
			enabledpopups=0;
		}
	};


	//useful stuff here (??? was: işe yarar şeyler bura)
        //&v should be used instead of , (was: , yerine &v kullanılmalı)
	String[] decode(String encoded){
		String[] decoded=(encoded).replaceAll("&","&a").split(",");
		for(int i=0;i<decoded.length;i++){
			decoded[i]=decoded[i].replaceAll("&a","&").replaceAll("&v",",").replaceAll("&V",",");
		}
		return decoded;
	}
	public void vibrate(){
		if(isVibrationEnabled){
			vib.vibrate(vibration);
		}
	}
	void playClick(View p1){
		if(((key)p1).getTag()!=null){
			playClick(Integer.parseInt("0"+((key)p1).getTag().toString()));
		}else{
			playClick(((key)p1).getText().toString().toLowerCase().toCharArray()[0]);
		}
	}
	void playClick() {
		playClick(0);
	}
	void playClick(int keyCode) {
		if(isAudioEffectEnabled){
			try {
				switch (keyCode) {
					case 32:
					am.playSoundEffect(AudioManager.FX_KEYPRESS_SPACEBAR);
					break;
					case 19:
					case 20:
					case 21:
					case 22:
					am.playSoundEffect(AudioManager.FX_KEYPRESS_RETURN);
					break;
					case 67:
					am.playSoundEffect(AudioManager.FX_KEYPRESS_DELETE);
					break;
					case 66:
					am.playSoundEffect(AudioManager.FX_KEY_CLICK);
					break;
					default:
					am.playSoundEffect(AudioManager.FX_KEYPRESS_STANDARD);
				}
			} catch (Exception e) {
			}
		}
	}
	public String[][][] UpperCase(String[][][] array){
		for(int i=0;i<array.length;i++){
			for(int j=0;j<array[i].length;j++){
				for(int k=0;k<array[i][j].length;k++){
					array[i][j][k]=array[i][j][k].toUpperCase(locale);
				}
			}
		}
		return array;
	}
	public String[][] UpperCase(String[][] array){
		for(int i=0;i<array.length;i++){
			for(int j=0;j<array[i].length;j++){
				array[i][j]=array[i][j].toUpperCase(locale);
			}
		}
		return array;
	}
	public String[][][] LowerCase(String[][][] array){
		for(int i=0;i<array.length;i++){
			for(int j=0;j<array[i].length;j++){
				for(int k=0;k<array[i][j].length;k++){
					array[i][j][k]=array[i][j][k].toLowerCase(locale);
				}
			}
		}
		return array;
	}
	public String[][] LowerCase(String[][] array){
		for(int i=0;i<array.length;i++){
			for(int j=0;j<array[i].length;j++){
				array[i][j]=array[i][j].toLowerCase(locale);
			}
		}
		return array;
	}



//" " is encoded with &b, "," is encoded with &v. (was: " " yerine &b "," yerine &v koyulur. )
//double space Produces " ". (was: Çift boşluk " " üretir.)
//each row becomes a row. (was: her satır birer row olur.)
	String[][] readData(String data){
		String[] lines=data.split("\n");
		String[][] readed=new String[lines.length][];
		for(int i=0;i<lines.length;i++){
			readed[i]=lines[i].split(" ");
			for(int j=0;j<readed[i].length;j++){
				readed[i][j].replaceAll("&v",",").replaceAll("&b"," ").replaceAll("&a","&");
				if(readed[i][j].length()<1){
					readed[i][j]=" ";
				}
			}
		}
		return readed;
	}
	public int getNavBarSize(){
		int resourceId = getResources().getIdentifier("navigation_bar_height", "dimen", "android");
		if (resourceId > 0 && getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
			navBarHeight = getResources().getDimensionPixelSize(resourceId);
		}
		if (!hasNavBar(getResources())) {
			navBarHeight=0;
		}
		if(navBarHeight<dpx(48) && navBarHeight!=0){
			navBarHeight=dpx(44);
		}
		return navBarHeight;	
	}
	public View getNavbarView() {
		return new View(ctx);
	}
	public boolean hasNavBar (Resources resources) {
		boolean hasNavigationBar = true;
		int id = resources.getIdentifier("config_showNavigationBar", "bool", "android");
		if (id > 0) {
			hasNavigationBar = resources.getBoolean(id);
		}
		try {
			Class systemPropertiesClass = Class.forName("android.os.SystemProperties");
			String navBarOverride = (String) systemPropertiesClass.getMethod("get", String.class).invoke(systemPropertiesClass, "qemu.hw.mainkeys");
			if ("1".equals(navBarOverride)) {
				hasNavigationBar = false;
			} else if ("0".equals(navBarOverride)) {
				hasNavigationBar = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hasNavigationBar;
	}

}
