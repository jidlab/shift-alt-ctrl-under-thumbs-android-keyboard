package org.droidtr.keyboard;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.graphics.drawable.*;
import android.os.Handler;
import android.os.Vibrator;
import android.view.View.*;
import android.view.View;
import android.view.*;
import android.widget.*;
import android.graphics.*;
import android.content.*;
import android.content.res.*;
import org.droidtr.keyboard.*;
import java.io.*;
public class pref extends Activity {
	@Override
	protected void onCreate(Bundle b) {
		super.onCreate(b);
		PrefView pref=new PrefView(this);
		if (Build.VERSION.SDK_INT > 21) {
			setTheme(android.R.style.Theme_Material_Light_NoActionBar);
		} else if (Build.VERSION.SDK_INT > 14) {
			setTheme(android.R.style.Theme_Holo_Light_NoActionBar);
		} else {
			setTheme(android.R.style.Theme_NoTitleBar);
		}
		if(Build.VERSION.SDK_INT>11){
			getWindow().setStatusBarColor(Color.parseColor("#00abc3"));
			getWindow().setNavigationBarColor(Color.parseColor("#00bcd4"));
		}
		setContentView(pref);
	}
	@Override
	protected void onResume(){
		onCreate(new Bundle());
	}
}

class PrefView extends LinearLayout{
	Context ctx=null;
	ScrollView sw=null;
	getSettings getSettings=null;
	util u=null;
	LinearLayout.LayoutParams lp=null;
	LinearLayout.LayoutParams lf=null;
	SharedPreferences read = null;
	SharedPreferences.Editor edit = null;
	setVibrateView svw=null;
	int tmp=0;
	Drawable gd=null;
	Drawable gd2=null;
	ColorPicker cp=null;
	DrawableTables dtws=null;
	Drawable dreset=null;
	boolean[] vars=new boolean[50];
	int mvar=0;
	PrefView(Context c){
		super(c);
		ctx=c;
		getSettings = new getSettings(c);
		u=new util();
		u.radius=getSettings.defaultButtonRadius;
		u.stroke=getSettings.defaultButtonStroke;
		dreset=getResources().getDrawable(R.drawable.reset);
		dreset.setColorFilter(Color.parseColor("#00bcd4"), PorterDuff.Mode.SRC_IN);
		gd = u.gd(Color.parseColor("#00bcd4"));
		read= c.getSharedPreferences("key", c.MODE_PRIVATE);
		edit=read.edit();
		lp = new LinearLayout.LayoutParams(-1,-2,1.0f);
		lf = new LinearLayout.LayoutParams(-1,-1,1.0f);
		sw = new ScrollView(c);
		sw.setLayoutParams(lp);
		this.setOrientation(LinearLayout.VERTICAL);
		this.addView(actionBar());
		RelativeLayout rl = new RelativeLayout(c);
		this.addView(rl);
		LinearLayout ll=new LinearLayout(c);
		this.setLayoutParams(lp);
		sw.addView(ll);
		sw.setPadding(dpx(12),dpx(12),dpx(12),dpx(12));
		cp =new ColorPicker(ctx);
		dtws = new DrawableTables(ctx);
		ll.setOrientation(LinearLayout.VERTICAL);

		//color adjustment (was: renk ayarı)
		ll.addView(dtws);

		//Keyboard interface selection (was: Klavye arayüzü seçimi)
		ll.addView(getLabel("Keyboard Layout"));
		ll.addView(getSeekSelect("keyboardLayout",new String[]{"TRQ","TRF","ENQ","TRK","TRM","RU","AR","GR","MC","BF","TRM!"},0));
		ll.addView(getLabel("Vibration Time"));
		svw=new setVibrateView(c);
		//vibration settings (was: titreşim ayarları)
		ll.addView(svw);

		//Button functions (was: fonksiyonları)
		ll.addView(getOnOff("butfunc","Button functions",getSettings.butfunc));
		//Navbar coloring (was: renklendirme)
		ll.addView(getOnOff("navbar","Navigation bar colorize",getSettings.isNavbarEnable));
		ll.addView(getOnOff("emoji","Emoji library",getSettings.isEmoji));
		ll.addView(getOnOff("isTabView","Top symbols",getSettings.isTabView));
		ll.addView(getOnOff("otobuyuk","Auto capitalization",getSettings.otobuyuk));
		ll.addView(getOnOff("mute","Mute keyboard sounds",!getSettings.isAudioEffectEnabled));
		ll.addView(getOnOff("vibenable","Keyboard vibration",getSettings.isVibrationEnabled));
		if(Build.VERSION.SDK_INT >= 23){
			ll.addView(getOnOff("isTablet","Tablet mode",getSettings.isTablet));
		}


		//height factor (was: yükseklik faktörü)
		ll.addView(getLabel("Heigh factor"));
		ll.addView(getSeekBar("heightFactor",100,45));
		//long press time (was: uzun basma süresi)
		ll.addView(getLabel("Long press time (ms)"));
		ll.addView(getSeekBar("popupDuration",500,250));
		ll.addView(getLabel("Key repeat time (ms)"));
		ll.addView(getSeekBar("repeatDuration",500,200));
		ll.addView(getLabel("Stroke size"));
		ll.addView(getSeekBar("strokeWidth",50,getSettings.defaultButtonStroke));
		ll.addView(getLabel("Primary text size"));
		ll.addView(getSeekBar("textSizeModifier",100,(int)(100*getSettings.primarySize)));
		ll.addView(getLabel("Secondary text size"));
		ll.addView(getSeekBar("secondTextSizeModifier",40,(int)(100*getSettings.secondarySize)));
		ll.addView(getLabel("Radius size"));
		ll.addView(getSeekBar("radius",50,getSettings.defaultButtonRadius));
		ll.addView(getLabel("Space length"));
		ll.addView(getSeekBar("spaceLength",100,getSettings.spaceLength));

		this.setBackgroundColor(Color.WHITE);
		this.addView(sw);

	}
	public static float dp(float px){
		return Resources.getSystem().getDisplayMetrics().density * px;
	}
	public static int dpx(float px){
		return (int)(Resources.getSystem().getDisplayMetrics().density * px);
	}
	TextView getLabel(String label){
		return getLabel(label,Color.GRAY,dpx(6));
	}
	TextView getLabel(String label,int col,int size){
		TextView b = new TextView(ctx);
		b.setLayoutParams(lp);
		b.setText(label);
		b.setTypeface(Typeface.DEFAULT_BOLD);
		b.setPadding(dpx(6),dpx(6),dpx(6),dpx(6));
		b.setTextColor(col);
		b.setLayoutParams(lp);
		if(size!=0){
			b.setTextSize(size);
		}
		return b;
	}
	LinearLayout getSeekSelect(String value, String[] array,int def){
		LinearLayout sl=new LinearLayout(ctx);
		sl.setLayoutParams(lp);
		SeekBar s = new SeekBar(ctx);
		TextView t=new TextView(ctx);
		Button reset = new Button(ctx);
		reset.setBackgroundDrawable(dreset);
		reset.setLayoutParams(new LinearLayout.LayoutParams(dpx(32),dpx(32)));
		sl.addView(reset);
		sl.addView(s);
		sl.addView(t);
		sl.setGravity(Gravity.CENTER);
		s.setLayoutParams(lp);
		s.setMax(array.length-1);
		tmp=read.getInt(value,0);
		if(Build.VERSION.SDK_INT>11){
			s.getProgressDrawable().setColorFilter(Color.parseColor("#00bcd4"), PorterDuff.Mode.SRC_IN);
			s.getThumb().setColorFilter(Color.parseColor("#00bcd4"), PorterDuff.Mode.SRC_IN);
		}
		s.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				t.setText(array[progress]+"");
				tmp=progress;
			}

			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			public void onStopTrackingTouch(SeekBar seekBar) {
				edit.putInt(value,tmp);
			}
		});
		reset.setOnClickListener(new OnClickListener(){
			public void onClick(View p1){
				s.setProgress(def);
				edit.putInt(value,def);
			}
		});
		t.setText(array[read.getInt(value,0)]+"");
		s.setProgress(read.getInt(value,0));
		return sl;
	}
	LinearLayout getSeekBar(String value,int max,int def){
		LinearLayout sl=new LinearLayout(ctx);
		sl.setLayoutParams(lp);
		final SeekBar s = new SeekBar(ctx);
		TextView t=new TextView(ctx);
		Button reset = new Button(ctx);
		sl.addView(reset);
		reset.setBackgroundDrawable(dreset);
		reset.setLayoutParams(new LinearLayout.LayoutParams(dpx(32),dpx(32)));
		sl.addView(s);
		sl.addView(t);
		sl.setGravity(Gravity.CENTER);
		s.setLayoutParams(lp);
		s.setMax(max);
		tmp=read.getInt(value,def);
		if(Build.VERSION.SDK_INT>11){
			s.getProgressDrawable().setColorFilter(Color.parseColor("#00bcd4"), PorterDuff.Mode.SRC_IN);
			s.getThumb().setColorFilter(Color.parseColor("#00bcd4"), PorterDuff.Mode.SRC_IN);
		}
		s.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				tmp=progress;
				t.setText(tmp+"");
			}

			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			public void onStopTrackingTouch(SeekBar seekBar) {
				edit.putInt(value,tmp);
			}
		});
		reset.setOnClickListener(new OnClickListener(){
			public void onClick(View p1){
				s.setProgress(def);
				edit.putInt(value,def);
			}
		});
		s.setProgress(read.getInt(value,def));
		return sl;
	}
	LinearLayout getOnOff(String feature,String name,boolean def){
		final int index=mvar;
		vars[mvar]=def;
		mvar++;
		LinearLayout oll=new LinearLayout(ctx);
		LinearLayout ol=new LinearLayout(ctx);
		ol.setBackgroundDrawable(u.gd(Color.parseColor("#00bcd4"),Color.parseColor("#00bcd4"),100,10));
		Button on=new Button(ctx);
		Button off=new Button(ctx);
		ol.setOrientation(LinearLayout.HORIZONTAL);
		oll.setOrientation(LinearLayout.HORIZONTAL);
		ol.setLayoutParams(new LinearLayout.LayoutParams(dpx(60),dpx(35)));
		ol.setPadding(dpx(5),dpx(5),dpx(5),dpx(5));
		on.setLayoutParams(new LinearLayout.LayoutParams(dpx(25),dpx(25)));
		off.setLayoutParams(new LinearLayout.LayoutParams(dpx(25),dpx(25)));
		oll.setLayoutParams(lp);
		if(read.getBoolean(feature,vars[index])){
			if(feature == "vibenable"){
				svw.v.setEnabled(true);
			}
			off.setBackgroundDrawable(u.gd(Color.WHITE,Color.WHITE,100,10));
			on.setBackgroundDrawable(u.gd(Color.TRANSPARENT,Color.TRANSPARENT,0,10));
			ol.setBackgroundDrawable(u.gd(Color.parseColor("#00bcd4"),Color.parseColor("#00bcd4"),100,10));
		}else{
			off.setBackgroundDrawable(u.gd(Color.TRANSPARENT,Color.TRANSPARENT,0,10));
			on.setBackgroundDrawable(u.gd(Color.WHITE,Color.WHITE,100,10));
			ol.setBackgroundDrawable(u.gd(Color.GRAY,Color.GRAY,100,10));
			if(feature == "vibenable"){
				svw.v.setEnabled(false);
			}
		}
		OnClickListener onoff = new OnClickListener() {
			public void onClick(View P1){
				vars[index]=!vars[index];
				if(vars[index]){
					if(feature == "vibenable"){
						svw.v.setEnabled(true);
					}
					off.setBackgroundDrawable(u.gd(Color.WHITE,Color.WHITE,100,10));
					on.setBackgroundDrawable(u.gd(Color.TRANSPARENT,Color.TRANSPARENT,0,10));
					ol.setBackgroundDrawable(u.gd(Color.parseColor("#00bcd4"),Color.parseColor("#00bcd4"),100,10));
				}else{
					off.setBackgroundDrawable(u.gd(Color.TRANSPARENT,Color.TRANSPARENT,0,10));
					on.setBackgroundDrawable(u.gd(Color.WHITE,Color.WHITE,100,10));
					ol.setBackgroundDrawable(u.gd(Color.GRAY,Color.GRAY,100,10));
					if(feature == "vibenable"){
						svw.v.setEnabled(false);
					}
				}
				edit.putBoolean(feature,vars[index]);
			}
		};
		on.setOnClickListener(onoff);
		off.setOnClickListener(onoff);
		oll.setOnClickListener(onoff);
		ol.addView(on);
		ol.addView(off);
		oll.addView(getLabel(name));
		oll.addView(ol);
		oll.setGravity(Gravity.CENTER);
		oll.setPadding(dpx(6),dpx(6),dpx(6),dpx(6));
		return oll;

	}
	LinearLayout actionBar(){
		LinearLayout sl=new LinearLayout(ctx);
		sl.setLayoutParams(new LinearLayout.LayoutParams(-1,dpx(48)));
		sl.addView(getLabel("  Settings",Color.WHITE,dpx(8)));
		sl.setGravity(Gravity.CENTER);
		Button ok = new Button(ctx);
		Button pick = new Button(ctx);
		pick.setOnClickListener(imagePick());
		pick.setOnLongClickListener(imagePickLong());
		ok.setOnClickListener(new OnClickListener() {
			public void onClick(View P1){
				edit.commit();
				System.exit(0);
			}
		});
		ok.setLayoutParams(new LinearLayout.LayoutParams(dpx(42),dpx(42)));
		ok.setBackgroundResource(R.drawable.save);
		ok.setPadding(5,5,5,5);
		pick.setLayoutParams(new LinearLayout.LayoutParams(dpx(42),dpx(42)));
		pick.setBackgroundResource(R.drawable.image);
		pick.setPadding(5,5,5,5);
		sl.addView(pick);
		sl.addView(ok);
		sl.setBackgroundColor(Color.parseColor("#00bcd4"));
		return sl;
	}
	class DrawableTables extends LinearLayout{
		Context ctx=null;
		HorizontalScrollView llsw=null;
		int current=0;
		String currentFeature="";
		int colors[][]=new int[10][3];
		DrawableTables(Context c){
			super(c);
			ctx=c;
			LinearLayout hsv1ll=new LinearLayout(c);
			hsv1ll.addView(getFeatureButton("bg",getSettings.bg,"Button"));
			hsv1ll.addView(getFeatureButton("bgon",getSettings.bgon,"Button On"));
			hsv1ll.addView(getFeatureButton("bgdown",getSettings.bgdown,"Button Down"));
			hsv1ll.addView(getFeatureButton("primbut",getSettings.primaryButtonColors,"Primary Button"));
			hsv1ll.addView(getFeatureButton("secondbut",getSettings.secondaryButtonColors,"Secondary Button"));
			hsv1ll.addView(getLabel("||"));
			hsv1ll.addView(getFeatureButton("color",getSettings.keyboardBackground,"Background"));
			hsv1ll.addView(getFeatureButton("tabViewColor",getSettings.tabViewColor,"Terminal Helper"));
			hsv1ll.addView(getFeatureButton("butcolor",getSettings.defaultButtonColor,"Primary Text"));
			hsv1ll.addView(getFeatureButton("butseccolor",getSettings.secondaryButtonColor,"Secondary Text"));
			this.setOrientation(LinearLayout.VERTICAL);
			cp.setLayoutParams(lp);
			llsw = new HorizontalScrollView(ctx);
			llsw.addView(hsv1ll);
			this.addView(cp);
			this.addView(llsw);

		}

		public Button getFeatureButton(String feature,int c,String name){
			return getFeatureButton(feature,new int[]{c},name);
		}
		public Button getFeatureButton(String feature,int[] c,String name){
			Button bg=new Button(ctx);
			bg.setBackgroundDrawable(u.gd(c,Color.TRANSPARENT));
			bg.setOnClickListener(getFeature(feature,current));
			colors[current]=new int[3];
			if(c.length==1){
				for(int i=0;i<colors[current].length;i++){
					colors[current][i]=c[0];
				}		
			}else{
				for(int i=0;i<c.length;i++){
					colors[current][i]=c[i];
				}
			}
			current++;
			currentFeature=feature;
			bg.setOnLongClickListener(getFeatureLong());
			bg.setTextColor(reverseColor(c[0]));
			bg.setShadowLayer(3,0,0,Color.WHITE);
			bg.setText(name);
			bg.setTag(c[0]+"");
			return bg;
		}
		public Button getFeatureColor(int num){
			Button bg=new Button(ctx);
			bg.setOnClickListener(getFeatureColorSet(num));
			bg.setOnLongClickListener(getFeatureLong());
			bg.setShadowLayer(3,0,0,Color.WHITE);
			return bg;
		}
		public OnLongClickListener getFeatureLong(){
			return new OnLongClickListener() {
   				public boolean onLongClick(View v){
					cp.setColor(Integer.parseInt(v.getTag().toString()));
					return true;
				}
			};
		}
		public OnClickListener getFeatureColorSet(int num){
			return new OnClickListener(){
				public void onClick(View P1){
					int c=cp.getColor();
					P1.setTag(c+"");
					((Button)P1).setTextColor(reverseColor(c));
					P1.setBackgroundDrawable(u.gd(c,Color.TRANSPARENT));
					String f="";
					colors[current][num]=c;
				}
			};
		}
		public OnClickListener getFeature(String feature,int num){
			return new OnClickListener(){
				public void onClick(View P1){
					int c=cp.getColor();
					currentFeature=feature;
					P1.setTag(c+"");
					if(feature == "color" || feature == "tabViewColor" || feature == "butcolor" || feature == "butseccolor"){
						if(feature == "color"){
							if(getSettings.keyboardBackgroundDrawable==null){
								llsw.setBackgroundColor(c);
							}
						}
					    edit.putInt(feature,c);
					    P1.setBackgroundDrawable(u.gd(c,Color.TRANSPARENT));
					((Button)P1).setTextColor(reverseColor(c));
					}else{
						edit.putString(feature,+Color.alpha(c)+","+Color.red(c)+","+Color.green(c)+","+Color.blue(c));
						P1.setBackgroundDrawable(u.gd(c,Color.TRANSPARENT));
					}
				}
			};
		}
	}
	class setVibrateView extends LinearLayout{
		boolean pause = false;
		TextView t=null;
		public SeekBar v=null;
		int tmp=0;
		Vibrator vib=null;
		boolean mute=false;
		boolean vibenable=true;
		setVibrateView(Context c){
			super(c);
			vibenable=read.getBoolean("vibenable", true);
			vib = (Vibrator) c.getSystemService(Context.VIBRATOR_SERVICE);
			LinearLayout ll = new LinearLayout(c);
			ll.setGravity(Gravity.CENTER);
			ll.setLayoutParams(new LinearLayout.LayoutParams(-1,-2));
			ll.setOrientation(LinearLayout.HORIZONTAL);
			v=new SeekBar(c);
			t=new TextView(c);
			Button reset = new Button(ctx);
			reset.setBackgroundDrawable(dreset);
			reset.setLayoutParams(new LinearLayout.LayoutParams(dpx(32),dpx(32)));
			ll.addView(reset);
			ll.addView(v);
			ll.addView(t);
			v.setMax(100);
			int def=read.getInt("vibtime", 35);
			v.setProgress(def);
			t.setText(def+ "");
			if(Build.VERSION.SDK_INT>11){
				v.getProgressDrawable().setColorFilter(Color.parseColor("#00bcd4"), PorterDuff.Mode.SRC_IN);
				v.getThumb().setColorFilter(Color.parseColor("#00bcd4"), PorterDuff.Mode.SRC_IN);
			}
			v.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					tmp=progress;
					update();
				}

				public void onStartTrackingTouch(SeekBar seekBar) {
				}

				public void onStopTrackingTouch(SeekBar seekBar) {
					edit.putInt("vibtime", tmp);
				}
			});
			ll.setLayoutParams(lp);
			v.setLayoutParams(lp);
			reset.setOnClickListener(new OnClickListener(){
				public void onClick(View p1){
					v.setProgress(35);
					edit.putInt("vibtime", 35);
				}
			});
			this.setLayoutParams(lp);
			this.addView(ll);
		}
		protected void update() {
			t.setEnabled(vibenable);
			v.setEnabled(vibenable);
				t.setText(tmp + "");
			if (vibenable) {
				vib.vibrate(tmp);
			}
		}
	}
	public OnClickListener imagePick(){
		return new OnClickListener(){
			public void onClick(View p1){
				Intent i=new Intent(ctx,image.class);
				ctx.startActivity(i);
			}
		};
	}
	public OnLongClickListener imagePickLong(){
		return new OnLongClickListener(){
			public boolean onLongClick(View p1){
				(new File(ctx.getFilesDir() + "/image.png")).delete();
				return true;
			}
		};
	}
	int reverseColor(int c){
		return Color.rgb(255-Color.green(c),255-Color.blue(c),255-Color.red(c));
	}
}

