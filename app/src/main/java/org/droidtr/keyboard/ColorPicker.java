package org.droidtr.keyboard;

import java.io.*;  
import android.app.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.os.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;
import android.content.*;
import android.content.res.*;

public class ColorPicker extends LinearLayout{
	int c=Color.RED;
	int cf=Color.RED;
	LinearLayout i=null;
	TextView hex=null;
	int a=255;
	Context ctx=null;
	SeekBar alpha=null;
	public Button okay=null;
	GradientDrawable gd=null;
	Bitmap bmp=null;
	private LinearLayout.LayoutParams llp=new LinearLayout.LayoutParams(-1,-1,1.0f);
	ColorPicker(Context context){
		super(context);
		ctx=context;
		i = new LinearLayout(ctx);
		final LinearLayout ix = new LinearLayout(ctx);
		final LinearLayout bottomlayer = new LinearLayout(ctx);
		final LinearLayout hexlayer = new LinearLayout(ctx);
		okay=new Button(ctx);
		hex=new TextView(ctx);
		okay.setLayoutParams(llp);
		hex.setLayoutParams(llp);
		hex.setGravity(Gravity.CENTER);
		alpha=new SeekBar(ctx);
		alpha.setMax(255);
		ix.setLayoutParams(llp);
		i.setLayoutParams(llp);
		bottomlayer.setLayoutParams(new LinearLayout.LayoutParams(-1,dpx(40)));
		hexlayer.setLayoutParams(llp);
		this.addView(i);
		i.setOrientation(LinearLayout.VERTICAL);
		i.addView(bottomlayer);
		i.addView(alpha);
		i.addView(ix);
		bottomlayer.addView(okay);
		bottomlayer.addView(hexlayer);
		gd = new GradientDrawable();
		gd.setColor(Color.BLACK);
		gd.setCornerRadius(14);
		hex.setBackgroundDrawable(gd);
		hexlayer.addView(hex);
		this.setPadding(dpx(6),dpx(6),dpx(6),dpx(6));
		if((new File("/data/data/org.droidtr.keyboard/hsv.png")).exists()){
			bmp=BitmapFactory.decodeFile("/data/data/org.droidtr.keyboard/hsv.png");
		}else{
			bmp = Bitmap.createBitmap(365,511,Bitmap.Config.ARGB_8888);
			for(int k=0;k<365;k++){
				for(int j=0;j<=255;j++){
					bmp.setPixel(k,j,(int)Color.HSVToColor(new float[]{(float)k,(float)j/255f,1f}));
				}
				for(int j=0;j<=255;j++){
					bmp.setPixel(k,j+255,(int)Color.HSVToColor(new float[]{(float)k,1f,(float)(255-j)/255f}));
				}
			}
			try{
				FileOutputStream fos = new FileOutputStream("/data/data/org.droidtr.keyboard/hsv.png");
				bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
			}catch(Exception e){}
		}
		ix.setOnTouchListener(new LinearLayout.OnTouchListener(){
			public boolean onTouch(View p1, MotionEvent event){
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
					case MotionEvent.ACTION_MOVE:
					case MotionEvent.ACTION_UP:
					int x=(int)((365*event.getX())/p1.getWidth());
					int y=(int)((511*event.getY())/p1.getHeight());
					if(x<0){x=0;}else if(x>=365){x=364;}
					if(y<0){y=0;}else if(y>=511){y=510;}
					c=bmp.getPixel(x,y);
					cf=bmp.getPixel(x,255);
					update(a);
				}
				return true;
			}
		});
		alpha.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				a=progress;
				update(progress);
			}

			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			public void onStopTrackingTouch(SeekBar seekBar) {
			}
		});
		alpha.setProgress(255);
		ix.setBackgroundDrawable(new BitmapDrawable(ctx.getResources(), bmp));
		update(255);
	}
	private void update(int aaa){
		c=Color.argb(aaa,Color.red(c),Color.green(c),Color.blue(c));
		gd = new GradientDrawable();
		gd.setColor(c);
		gd.setCornerRadius(14);
		gd.setStroke(3,Color.BLACK);
		okay.setBackgroundDrawable(gd);
		if(Build.VERSION.SDK_INT>11){
			alpha.getProgressDrawable().setColorFilter(cf, PorterDuff.Mode.SRC_IN);
			alpha.getThumb().setColorFilter(cf, PorterDuff.Mode.SRC_IN);
		}
		hex.setText(String.format("#%08X", 0xFFFFFFFF & c)+"\n"+Color.alpha(c)+" - "+Color.red(c)+" - "+Color.green(c)+" - "+Color.blue(c));
		hex.setTextColor(cf);
	}
	public int getColor(){
		return c;
	}
	public void setColor(int color){
		c=color;
		cf=Color.argb(255,Color.red(c),Color.green(c),Color.blue(c));
		alpha.setProgress(0);
		alpha.setProgress(Color.alpha(c));
	}

	public static float dp(float px){
		return Resources.getSystem().getDisplayMetrics().density * px;
	}
	public static int dpx(float px){
		return (int)(Resources.getSystem().getDisplayMetrics().density * px);
	}
}
