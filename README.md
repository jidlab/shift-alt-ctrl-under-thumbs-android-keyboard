Onscreen keyboard implementing the Azerty-Lafayette layout of https://github.com/jidhub/thumb-external-keyboard-layouts (which implements this layout for android bluetooth keyboard, and for linux wayland).q

This project ensures that the two left and right keys around the space key, close to my thumbs, have this effect:

Alt Shift Space AltGr Control

I don't use keys ESC, 1, 6, 7, Y, B, BACKSPACE, and the arrows because they are too far from the rest position of my fingers. I type them with respectively AltGr+q, AltGr+Shift+i, AlgGr+Shift+v, AlgGr+Shift+f, AlgGr+u, AlgGr-n, AltGr-s, AltGr-h, AltGr-j, AltGr-k and AltGr-l. I type AltGr+i, AltGr+g, AltGr+f, AltGr+d and AltGr+Shift+d for the other frequent uses of these keys, namely: &, -, è, | and `

AltGr+Shift on number keys mostly types the characters shown on  qwerty keyboard, useful when, tired, I use this keyboard map on external keyboards.

Warning, to this day I am the only user and this is the first time I am sharing this project, so please give feedbacks to help me.

My philosophy is that a the learning curve for an azerty user is manageable.

Another feature is that you can type more characters by swiping the keys towards the corners.

This application was originally designed for programmers using Termux.
Now perfect for everyday use.

This application contains no ads, doesn't make any network requests and is Open Source.

Usage: to apply the symbols located in the corners of each key, slide your finger in the direction of the symbols. For example, the Settings are opened by sliding in the left down corner.

| <img src="/fastlane/metadata/android/en-US/images/phoneScreenshots/1.png" alt="Screenshot-1" /> | <img src="/fastlane/metadata/android/en-US/images/phoneScreenshots/2.png" alt="Screenshot-2"/> | <img src="/fastlane/metadata/android/en-US/images/phoneScreenshots/3.png" alt="Screenshot-3"/> |
| --- | --- | --- |
| <img src="/fastlane/metadata/android/en-US/images/phoneScreenshots/4.png" alt="Screenshot-4" /> | <img src="/fastlane/metadata/android/en-US/images/phoneScreenshots/5.png" alt="Screenshot-5" /> | <img src="/fastlane/metadata/android/en-US/images/phoneScreenshots/6.png" alt="Screenshot-6" /> |

## Contributing

For instructions on building the application, see
[Contributing](CONTRIBUTING.md).
