> The only compile environment tested is: 

    android-studio Android Studio Dolphin | 2021.3.1 Patch 1
    Build #AI-213.7172.25.2113.9123335, built on September 30, 2022
    Runtime version: 11.0.13+0-b1751.21-8125866 amd64
    VM: OpenJDK 64-Bit Server VM by JetBrains s.r.o.
    Linux 5.15.79
    GC: G1 Young Generation, G1 Old Generation
    Memory: 1280M
    Cores: 6
    Registry:
        external.system.auto.import.disabled=true
        ide.text.editor.with.preview.show.floating.toolbar=false
    It downloaded https://services.gradle.org/distributions/gradle-7.0.2-bin.zip

> Optional: fork this project to be able to Pull-Request it.

> Optional: if your android-studio version is older than my, consider reverting (the commit upgrading gradle)[https://gitlab.com/jidlab/shift-alt-ctrl-under-thumbs-android-keyboard/-/commit/61356e1b04a9e3dcd03d02d2112786ec28d2c11b].

> Download the sources with git clone. Optional: use the ssh link to be able to use your own editors.

> I personnaly cloned it on the termux app of my fold 3 (and access it avoiding adb shell as this cannot share the connection with android-studio), then use that vim noremap to upload it to the NixOS linux box:

`noremap à :w<bar>!scp mol8_wifi:shift-alt-ctrl-under-thumbs-android-keyboard/app/src/main/java/org/droidtr/keyboard/IME.java /data/data/com.termux/files/home/p/r/previous.keyboard ; rsync -avz ~/p/r/K/INFORMATIQUE/android/shift-alt-ctrl-under-thumbs-android-keyboard mol8_wifi: ; cvs commit -m sauver`

> Then run it in Android Studio. My Fold3 shows the message on first installation:

Google Play Protect 

Unsafe app blocked

This app was built for an older version of Android and doesn't include the latest privacy protection

More details

Click to expand

> I ask for more details, which adds

Installing this app may put your device at risk. Learn more about Play Protect.

Install anyway

> Then the app is launched. This lets you deploy selected color to various parts of the keyboard, select the english keyboard ENQ instead of the turkish default TRQ, disable vibration for keypresses, ... To save settings, pres the top right button, which exits the app.

> Then, in the settings, go to `General Managment/Language and keybard - Date and time/Keyboard list and default` and check `DroidTR IME`, which shows the following message:

Attention

This input method may be able to collect all the text you type, including personal data like passwords and credit card numbers. It comes from the app DroidTR IME. Use this input method?

> Then you get the message

Note: After a reboot, this app can't start until you unlock your phone

> Then in the settings, go to `General Managment/Language and keybard - Date and time/Keyboard list and default/Default keyboard` and select `DroidTR IME`. Now open any app with a text field, and you can test this app.
